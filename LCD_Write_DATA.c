/*! \file  LCD_Write_DATA.c
 *
 *  \brief Write data to the TFT
 *
 *
 *  \author jjmcd
 *  \date January 2, 2014, 8:34 AM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

//#include "class_attributes.h"
#include <stdint.h>


/*! LCD_Write_DATA - */

/*!
 *
 */
void LCD_Write_DATA(uint8_t VH,uint8_t VL)
{
//    sbi(P_RS, B_RS);
    setRS();
    LCD_Writ_Bus(VH,VL);
}

void LCD_Write_DATA16(uint16_t V)
{
//    sbi(P_RS, B_RS);
    setRS();
    LCD_Writ_Bus16(V);
}
