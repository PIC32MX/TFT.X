/*! \file  TFTGrange.c
 *
 *  \brief Set the X and Y axis range for graphs
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:54 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFTGinternal.h"


/*! TFTGrange - Set the X and Y axis range for graphs */

/*!
 *
 */
void TFTGrange(int nX0, int nX9, int nY0, int nY9 )
{
  nXmin = nX0;
  nXmax = nX9;
  nYmin = nY0;
  nYmax = nY9;
}
