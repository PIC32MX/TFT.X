/*! \file  TFTGinternal.h
 *
 *  \brief Private declarations for the graphing library
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 10:19 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TFTGINTERNAL_H
#define	TFTGINTERNAL_H

#ifdef	__cplusplus
extern "C"
{
#endif

#ifndef EXTERN
#define EXTERN extern
#endif

EXTERN unsigned int uScreenColor;
EXTERN unsigned int uFieldColor;
EXTERN unsigned int uAnnotColor;
EXTERN unsigned int uXLabelColor;
EXTERN unsigned int uYLabelColor;
EXTERN unsigned int uTitleColor;
EXTERN unsigned int uLineColor;
EXTERN unsigned int uCursorColor;
EXTERN int nXmin;
EXTERN int nXmax;
EXTERN int nYmin;
EXTERN int nYmax;
EXTERN char szTitle[32];
EXTERN char szXLabel[32];
EXTERN char szYLabel[32];
EXTERN int nLastX,nLastY;
EXTERN int nUseCursor;
EXTERN int nEraseLast;

#define TFTGXLEFT 41
#define TFTGXRIGHT 319
#define TFTGYTOP 21
#define TFTGYBOTTOM 212


#ifdef	__cplusplus
}
#endif

#endif	/* TFTGINTERNAL_H */

