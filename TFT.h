/*! \file  TFT.h
 *
 *  \brief Function prototypes and manifest constants for TFT.a
 *
 *
 *  \author jjmcd
 *  \date February 26, 2014, 2:44 PM
 */
/* Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#ifndef TFT_H
#define	TFT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "colors.h"

/* Conditionalized because some may be defined elsewhere */
#ifndef LEFT
#define LEFT 0
#endif
#ifndef RIGHT
#define RIGHT 9999
#endif
#ifndef CENTER
#define CENTER 9998
#endif
#ifndef LANDSCAPE
#define LANDSCAPE 1
#endif
#ifndef PORTATIT
#define PORTRAIT 0
#endif
#ifndef fontdatatype
#define fontdatatype uint8_t
#endif

extern const fontdatatype SmallFont[];
extern const fontdatatype BigFont[];
extern const fontdatatype SevenSegNumFont[];
extern const fontdatatype BoldSmall[];
extern const fontdatatype DJS[];
extern const fontdatatype DJSB[];

void TFTcircle( int, int, int );
void TFTclear();
void TFTclearRect(uint8_t,uint8_t,uint8_t,int,int,int,int);
void TFTfillCircle( int, int, int );
void TFTfillRoundRect(int, int, int, int);
void TFThLine( int, int, int );
void TFTinit( int );
void TFTline( int, int, int, int );
void TFTpixel( int, int );
void TFTprintChar( char, int, int );
void TFTprint( char *, int, int, int );
void TFTrotateChar(uint8_t, int, int, int, int );
void TFTroundRect(int, int, int, int);
void TFTsetBackColor(uint8_t, uint8_t, uint8_t);
void TFTsetBackColorX(unsigned int);
void TFTsetColor(uint8_t, uint8_t, uint8_t);
void TFTsetColorX( unsigned int x );
void TFTsetFont( const fontdatatype* );
void TFTsetTransparent( int );
void TFTvLine( int, int, int );
void TFTrect(int, int, int, int);
void TFTfillRect( int, int, int, int );
unsigned int TFTgetBackColor( void );
unsigned int TFTgetColor( void );
void TFTprintDec( int, int, int, int );
void TFTprintDecRight( int, int, int, int );
void TFTprintInt( int, int, int, int );
void TFTtransitionOpen( int );
void TFTtransitionClose( int );
void TFTtransitionWipe( int, int );
void SetupHardware( void );
void TFTputchTT( unsigned char );
void TFTputsTT( unsigned char * );




#ifdef	__cplusplus
}
#endif

#endif	/* TFT_H */

