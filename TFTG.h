/*! \file  TFTG.h
 *
 *  \brief Global declarations for the Graphing library
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 10:19 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TFTG_H
#define	TFTG_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! TFTGaddPoint - Add a point to an existing graph */
void TFTGaddPoint( int );
/*! TFTGannotateX - Annotate the X axis */
void TFTGannotateX(void);
/*! TFTGannotateY - Annotate the Y axis */
void TFTGannotateY(void);
/*! TFTGcolors - Set the graph colors */
void TFTGcolors( unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int );
/*! TFTGexpose - Draw the external, fixed parts of the graph */
void TFTGexpose(void);
/*! TFTGinit - Initialize the graph */
void TFTGinit(void);
void TFTGnoCursor( void );
void TFTGnoErase( void );
void TFTGrange( int, int, int, int );
/*! TFTGtitle - Set the title of the graph */
void TFTGtitle( char * );
/*! TFTGXlabel - Set the X-axis labelfor the graph */
void TFTGXlabel( char * );
/*! TFTGYlabel - Set the Y-axis labelfor the graph */
void TFTGYlabel( char * );

#ifdef	__cplusplus
}
#endif

#endif	/* TFTG_H */

