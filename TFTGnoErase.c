/*! \file  TFTGnoErase.c
 *
 *  \brief Do not erase previous graph trace
 *
 *
 *  \author jjmcd
 *  \date June 18, 2015, 11:49 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFTGinternal.h"

/*! TFTGnoErase - Do not erase previous graph trace */

/*!
 *
 */
void TFTGnoErase(void)
{
  nEraseLast = 0;
  nUseCursor = 0;

}
