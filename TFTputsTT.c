/*! \file  TFTputsTT.c
 *
 *  \brief Display a character string, teletype mode
 *
 *
 *  \author jjmcd
 *  \date June 22, 2015, 7:43 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFT.h"

/*! TFTputsTT - Display a string teletype mode */
/*!
 *
 */
void TFTputsTT(unsigned char *p)
{
  while (*p)
    {
      TFTputchTT(*p);
      p++;
    }

}
