/*! \file  TFTtransitions.c
 *
 *  \brief Clear the display with various animations
 *
 *
 *  \author jjmcd
 *  \date March 21, 2014, 3:57 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFT.h"
#include "TFThardware.h"
#include "class_attributes.h"

/*! Draw ever increasing rectangles until screen is filled with LINEN */
/*! The screen is cleared to BLACK and then rectangles of the same aspect
 *  ratio of the screen are drawn in LINEN, increasing in size until the
 *  screen is filled.  This gives the appearance of a growing rectangle
 *  until the screen is filled with the same color that will be the
 *  background for the following display.
 */
void TFTtransitionOpen( int color )
{
  int x,y;
  unsigned int saveC;

  saveC = TFTgetColor();

  TFTsetBackColorX(color);
  TFTsetColorX(color);

  for ( y=1; y<121; y++ )
    {
      x = (160 * y + 60) / 120;
      TFTfillRect( 159-x, 120-y, 160+x, 120+y );
    }
  TFTclear();
  //TFTfillRect(0,0,119,9);
  TFTline(0,0,319,0);
  TFTline(0,1,319,1);
  TFTsetColorX(saveC);
}



void TFTtransitionClose( int color )
{
    int i;
  unsigned int saveC;

  saveC = TFTgetColor();

    TFTsetColorX(color);
    for ( i=0; i<120; i++ )
    {
        TFTrect(i,i,319-i,239-i);
        TFTpixel(i,239-i);
    }
  TFTsetColorX(saveC);
}

/*! Transition to another screen */
/*! Slides the current screen to the right, then back, but as it
 *  moves in from the right, it has been replaced with the background
 *  color of the next screen.
 */
void TFTtransitionWipe( int color, int slow )
{
  int i;
  unsigned int oldcolor;
  unsigned int saveC;

  saveC = TFTgetColor();

  oldcolor = bch<<8 | bcl;
  TFTsetColorX(oldcolor);
  /* Note: Things get weird thwn the CS is manipulated right
   * around the LCD_Write.  Display gets shifted, but erasing
   * happens ahead of the shift */
  clearCS();
  for ( i=0; i<320; i++ )
    {
      TFTline(i,0,i,239);
      LCD_Write_COM_DATA(0x41,i);
      LCD_Write_COM(0x22);
      delay(slow);
    }
  setCS();

  TFTsetColorX(color);
  for ( i=320; i>0; i-- )
    {
      TFTline(319-i,0,319-i,239);
      clearCS();
      LCD_Write_COM_DATA(0x41,i);
      LCD_Write_COM(0x22);
      setCS();
      delay(slow);
    }
  clearCS();
  LCD_Write_COM_DATA(0x41,0);
  LCD_Write_COM_DATA(0x42,0);
  LCD_Write_COM(0x22);
  setCS();
  TFTsetBackColorX(color);
  TFTsetColorX(saveC);

}
