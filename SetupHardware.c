/*! \file  SetupHardware.c
 *
 *  \brief Sets up TRIS registers, etc.
 *
 *  \author jjmcd
 *  \date December 14, 2013, 9:43 AM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include <xc.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "../include/pps.h"


/*! sendToS17 - Send a word to the MCP23S17 */
void sendToS17( int, uint8_t, uint16_t );

/*! SetupHardware - Initialize the hardware registers */
/*! Set the TRIS bits for the 2 pins attached to LEDs, as well as
 *  the SPI chip select, clock and data out.  Set Chip Select and
 *  output high (inactive) before setting to be outputs.  Also
 *  initialize the SPI peripheral.
 */
void SetupHardware(void)
{
    int trash;

  /* TRIS bits for SPI pins (used later) */
  _TRISB13 = 0; // SDO
  _TRISA4 = 1;  // SDI
  _TRISB15 = 0; // SDC

  /* I/O expander #1 chip select */
  I1_CS = 1;
  I1_CS_TRIS = 0;

  /* I/O expander #2 chip select */
  I2_CS = 1;
  I2_CS_TRIS = 0;

  /* TFT chip select */
  setWR();
  T_WR_TRIS = 0;
  /* TFT RS */
  T_RS = 1;
  T_RS_TRIS = 0;

  /* Peripheral pin select - point SPI at some pins */
  /* Map SDO2 to RPB13 */
  RPB13R = OUT_FN_PPS2_SDO2 /* 0x0004 */;
  /* Map SDI2 to RPA4 - not actually used at this point */
  SDI2R =  IN_PIN_PPS3_RPA4 /* 0x0002 */;

  /* 1 - disable SPI interrupts */
  IEC1bits.SPI2TXIE   = 0;
  IEC1bits.SPI2EIE    = 0;
  /* 2 - Stop and reset the SPI peripheral */
  SPI2CONbits.ON      = 0;
  /* 3 - Clear (read) the receive buffer */
  trash               = SPI2BUF;
  /* 4 - Set enhanced buffer mode */
  SPI2CONbits.ENHBUF  = 1;
  /* 5 - no interrupts for now */
  /* 6 - Write the baud rate register */
  /* 50/(4+1)=10 <MCP23S17 spec fastest */
  SPI2BRG             = 4;
  /* 7 - Clear the SPIROV bit (rcv overflow) */
  SPI2STATbits.SPIROV = 0;
  /* 8 - Write desired settings to SPI2CON (master mode) */
  SPI2CONbits.MSTEN   = 1;
  /* Clock polarity */
  SPI2CONbits.CKP     = 0;
  /* Clock edge select */
  SPI2CONbits.CKE     = 0;
  /* Set up message length.  Note that although the datasheet
   * indicates both bits set qould be 24 bits, 32 bits is sent */
  SPI2CONbits.MODE32  = 1;  /* 24 bit data initially */
  SPI2CONbits.MODE16  = 1;
  /* 9 - Enable the SPI peripheral */
  SPI2CONbits.ON      = 1;

    /* Set MCP23S17 to ignore hardware address bits */
  sendToS17(0,S17_CONFA,IOCON_HAEN<<8|IOCON_HAEN);
  sendToS17(1,S17_CONFA,IOCON_HAEN<<8|IOCON_HAEN);
  /* Set MCP23S17 all latches high */
  sendToS17(0,S17_OLATA,0xffff);
  sendToS17(1,S17_OLATA,0xffff);
  /* Set #1 MCP23S17 all outputs */
  sendToS17(0,S17_IODIRA,0x0000);
  /* MCP23S17 #2 some inputs for touch */
  sendToS17(1,S17_IODIRA,TT_DIR);
  /* Enable MCP23S17 pull-up resistors */
  sendToS17(0,S17_GPPUA,0xffff);
  sendToS17(1,S17_GPPUA,0xffff);

  setCS();
  setRD();

}
