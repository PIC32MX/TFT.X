/*! \file  TFTpixel.c
 *
 *  \brief Draw a pixel on the TFT
 *
 *
 *  \author jjmcd
 *  \date January 8, 2014, 1:43 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include <xc.h>
#include <stdint.h>
#include "class_attributes.h"
#include "TFT.h"

#define word uint16_t

#define swap(type, i, j) {type t = i; i = j; j = t;}


void setPixel(word color)
{
    //LCD_Write_DATA((color>>8),(color&0xFF));        // rrrrrggggggbbbbb
    LCD_Write_DATA16(color);        // rrrrrggggggbbbbb
}

/*! drawPixel - */

/*!
 *
 */
void TFTpixel(int x, int y)
{
    clearCS();
    setXY(x, y, x, y);
    //setPixel((fch<<8)|fcl);
    setPixel( stForeground.fgcolor );
    setCS();
    clrXY();
}


