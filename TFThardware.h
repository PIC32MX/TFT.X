/*! \file TFThardware.h
 *
 * \brief Function prototypes for simple hardware manipulation
 *
 */
#ifndef TFTHARDWARE_H
#define	TFTHARDWARE_H

#ifndef word
#define word uint16_t
#endif

void setClear( word mask );
void clearRS( void );
void clearCS( void );
void clearRD( void );
void clearRST( void );
void setRS( void );
void setCS( void );
void setRD( void );
void setRST( void );
void setXY(word, word, word, word);
void clrXY( void );
void LCD_Writ_Bus(uint8_t,uint8_t );
void LCD_Write_COM(uint8_t );
void LCD_Write_COM_DATA(uint8_t, uint16_t );
void LCD_Write_DATA(uint8_t,uint8_t );

#endif
