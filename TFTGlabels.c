/*! \file  TFTGlabels.c
 *
 *  \brief Routines for supplying graph labels
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:51 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "TFTGinternal.h"


/*! TFTGtitle - Set the title of the graph */
void TFTGtitle( char *t )
{
  if ( t )
    strcpy(szTitle,t);
}

/*! TFTGXlabel - Set the X-axis labelfor the graph */
void TFTGXlabel( char *x )
{
  if (x)
    strcpy(szXLabel, x);
}

/*! TFTGYlabel - Set the Y-axis labelfor the graph */
void TFTGYlabel( char *y )
{
  if (y)
    strcpy(szYLabel, y);
}
