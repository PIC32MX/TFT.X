/*! \file  TFTclear.c
 *
 *  \brief Routines for clearing the screen
 *
 *  Module contains two functions for filling a rectangular area
 *  of the screen.  TFTclearRect() accepts r, g, b, colors and x,y
 *  extents of the area to be filled.  TFTclear floods the entire
 *  screen with the current background color.
 *
 *
 *  \author jjmcd
 *  \date February 25, 2014, 5:08 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */
#include <xc.h>
#include <stdint.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "TFT.h"


/*! TFTclearRect - Fill a rectangular area with color */

/*! Fills a rectangular area of the screen with color by writing
 *  directly to the hardware.
 *
 */
void TFTclearRect(uint8_t r, uint8_t g,uint8_t b,int x1,int y1,int x2,int y2)
{
  uint16_t colval;
  long i,n;

  n = ( (long)x2-(long)x1+1L ) * ( (long)y2-(long)y1+1L );
  colval = ( (r&0xf8)<<8 ) | ( (g&0xfc)<<3 ) | ( (b&0xf8) >> 3 );
  clearCS();
  setXY(x1,y1,x2,y2);
  setRS();
  sendToS17(0,S17_OLATA,colval);
  for ( i=0; i<n; i++ )
    {
      clearWR();
      //Nop();
      setWR();
      //Nop();
    }
  setCS();
}

/*! TFTclear - fill the screen with background color */

/*! Fills the entire screen with the current background color
 *  by directly writing to the hardware.
 *
 */
void TFTclear( void )
{
  uint16_t colval;
  long i,n;

  n = 76800;
  colval = ( bch<<8 ) | bcl ;
  clearCS();
  setXY(0,0,319,239);
  setRS();
  sendToS17(0,S17_OLATA,colval);
  for ( i=0; i<n; i++ )
    {
      clearWR();
      //Nop();
      setWR();
      //Nop();
    }
  setCS();
}
