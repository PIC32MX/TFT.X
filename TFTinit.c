/*! \file  TFTinit.c
 *
 *  \brief Initialize the TFT display
 *
 *
 *  \author jjmcd
 *  \date January 8, 2014, 1:43 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSmart 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include <xc.h>
#include <stdint.h>
#include "HardwareDefs.h"
#include "Delays.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "SSD1289defs.h"
#include "TFT.h"


void TFTinit(int orientation)
{
    /* These initial parts from constructor */
    disp_x_size=239;
    disp_y_size=319;
    display_transfer_mode=16;

    orient=orientation;
    /* For PIC32 _hw_special_init() is null */
    //_hw_special_init();

    //? Clear CS before init?
    //clearCS();

    //        sbi(P_RST, B_RST);
    setRST();
    delay(5); 
    //        cbi(P_RST, B_RST);
    clearRST();
    delay(15);
    //        sbi(P_RST, B_RST);
    setRST();
    delay(15);

    clearCS();

    LCD_Write_COM_DATA(SSD1289_OSCEN,0x0001);
    LCD_Write_COM_DATA(SSD1289_PWRC1,0xA8A4);
    LCD_Write_COM_DATA(SSD1289_PWRC2,0x0000);
    LCD_Write_COM_DATA(SSD1289_PWRC3,0x080C);
    LCD_Write_COM_DATA(SSD1289_PWRC4,0x2B00);
    LCD_Write_COM_DATA(SSD1289_PWRC5,0x00B7);
    LCD_Write_COM_DATA(SSD1289_DRVOC,0x2B3F);
    LCD_Write_COM_DATA(SSD1289_LCDAC,0x0600);
    LCD_Write_COM_DATA(SSD1289_SLEEP,0x0000);
    LCD_Write_COM_DATA(SSD1289_ENTRY,0x6070);
    LCD_Write_COM_DATA(SSD1289_CMPR1,0x0000);
    LCD_Write_COM_DATA(SSD1289_CMPR2,0x0000);
    LCD_Write_COM_DATA(SSD1289_HPORC,0xEF1C);
    LCD_Write_COM_DATA(SSD1289_VPORC,0x0003);
    LCD_Write_COM_DATA(SSD1289_DPYCT,0x0233);
    LCD_Write_COM_DATA(SSD1289_FCYCT,0x0000);
    LCD_Write_COM_DATA(SSD1289_GASSP,0x0000);
    LCD_Write_COM_DATA(0x41,0x0000);
    LCD_Write_COM_DATA(0x42,0x0000);
    LCD_Write_COM_DATA(0x48,0x0000);
    LCD_Write_COM_DATA(0x49,0x013F);
    LCD_Write_COM_DATA(0x4A,0x0000);
    LCD_Write_COM_DATA(0x4B,0x0000);
    LCD_Write_COM_DATA(0x44,0xEF00);
    LCD_Write_COM_DATA(0x45,0x0000);
    LCD_Write_COM_DATA(0x46,0x013F);
    LCD_Write_COM_DATA(SSD1289_GAM01,0x0707);
    LCD_Write_COM_DATA(SSD1289_GAM02,0x0204);
    LCD_Write_COM_DATA(SSD1289_GAM03,0x0204);
    LCD_Write_COM_DATA(SSD1289_GAM04,0x0502);
    LCD_Write_COM_DATA(SSD1289_GAM05,0x0507);
    LCD_Write_COM_DATA(SSD1289_GAM06,0x0204);
    LCD_Write_COM_DATA(SSD1289_GAM07,0x0204);
    LCD_Write_COM_DATA(SSD1289_GAM08,0x0502);
    LCD_Write_COM_DATA(SSD1289_GAM09,0x0302);
    LCD_Write_COM_DATA(SSD1289_GAM10,0x0302);
    LCD_Write_COM_DATA(SSD1289_RWDM1,0x0000);
    LCD_Write_COM_DATA(SSD1289_RWDM2,0x0000);
    LCD_Write_COM_DATA(0x25,0x8000); // There is no command 0x25(!)
    LCD_Write_COM_DATA(0x4f,0x0000);
    LCD_Write_COM_DATA(0x4e,0x0000);
    LCD_Write_COM(SSD1289_WRITE);



    setCS();

    TFTsetColor(255, 255, 255);
    TFTsetBackColor(0, 0, 0);
    cfont.font=0;
    _transparent = false;
}
