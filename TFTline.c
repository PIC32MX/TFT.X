/*! \file  TFTline.c
 *
 *  \brief Draw a line on the TFT
 *
 *
 *  \author jjmcd
 *  \date February 25, 2014, 7:46 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */
#include <xc.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"
#include "class_attributes.h"
#include "TFT.h"

#define true 1

//void _fast_fill_16( uint8_t, uint8_t, long );
void _fast_fill_1616( uint16_t, long );

/*! drawLine - */

/*!
 *
 */

void TFTline(int x1, int y1, int x2, int y2)
{
    unsigned int dx,dy;
    int xstep,ystep;
    unsigned int col,row;
    int t;

    if (y1==y2)
	TFThLine((int)x1, (int)y1, (int)x2-x1);
    else if (x1==x2)
	TFTvLine((int)x1, (int)y1, (int)y2-y1);
    else
	{
	    dx = (x2 > x1 ? x2 - x1 : x1 - x2);
	    xstep =  x2 > x1 ? 1 : -1;
	    dy = (y2 > y1 ? y2 - y1 : y1 - y2);
	    ystep =  y2 > y1 ? 1 : -1;
	    col = x1, row = y1;

	    clearCS();
	    if (dx < dy)
		{
		    t = - (dy >> 1);
		    while (true)
			{
			    setXY (col, row, col, row);
			    //LCD_Write_DATA ((fch), (fcl);
			    LCD_Write_DATA16 ( stForeground.fgcolor );
			    if (row == (unsigned)y2)
				return;
			    row += ystep;
			    t += dx;
			    if (t >= 0)
				{
				    col += xstep;
				    t   -= dy;
				}
			}
		}
	    else
		{
		    t = - (dx >> 1);
		    while (true)
			{
			    setXY (col, row, col, row);
			    //LCD_Write_DATA ((fch), (fcl));
			    LCD_Write_DATA16 ( stForeground.fgcolor );
			    if (col == (unsigned)x2)
				return;
			    col += xstep;
			    t += dy;
			    if (t >= 0)
				{
				    row += ystep;
				    t   -= dx;
				}
			}
		}
	    setCS();
	}
    clrXY();
}

void TFThLine(int x, int y, int l)
{
    if (l<0)
	{
	    l = -l;
	    x -= l;
	}
    clearCS();
    setXY(x, y, x+l, y);
    setRS();
    //_fast_fill_16((uint8_t)fch,(uint8_t)fcl,(long)l);
    _fast_fill_1616(stForeground.fgcolor,(long)l);
    setCS();
    clrXY();
}

void TFTvLine(int x, int y, int l)
{
    if (l<0)
	{
	    l = -l;
	    y -= l;
	}
    clearCS();
    setXY(x, y, x, y+l);
    setRS();
    //_fast_fill_16((uint8_t)fch,(uint8_t)fcl,(uint8_t)l);
    _fast_fill_1616(stForeground.fgcolor,(long)l);
    setCS();
    clrXY();
}

//void _fast_fill_16( uint8_t h, uint8_t l, long v )
//{
//    long i;
//
//    sendToS17(0,S17_OLATA,h<<8|l);
//    for ( i=0; i<v; i++ )
//      {
//        clearWR();
//        Nop();
//        setWR();
//        Nop();
//      }
//
//}

void _fast_fill_1616( uint16_t V, long v )
{
    long i;

    sendToS17(0,S17_OLATA,V);
    for ( i=0; i<v; i++ )
	{
	    clearWR();
	    Nop();
	    setWR();
	    Nop();
	}

}
