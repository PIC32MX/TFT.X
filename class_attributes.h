/*! \file  class_attributes.h
 *
 *  \brief Low-level private constants and structures
 *
 *  This header contains the low-level state for the TFT software.
 *
 *
 *  \author jjmcd
 *  \date January 2, 2014, 8:45 AM
 *
 * \todo Instantiate all globals in a single place
 */
/* Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#ifndef CLASS_ATTRIBUTES_H
#define	CLASS_ATTRIBUTES_H

#ifdef	__cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <stdbool.h>

#define byte uint8_t
#define boolean bool

#ifndef EXTERN
#define EXTERN extern
#endif

/*! Constant defining landscape orientation to TFTinit() and others */
#define LANDSCAPE 1
/*! Constant defining portrait orientation to TFTinit() and others */
#define PORTRAIT 0

/*! Structure defining a font */
struct _current_font
{
    uint8_t* font;      /*!< Pointer to font data */
    uint8_t x_size;     /*!< Horizontal size of a character cell */
    uint8_t y_size;     /*!< Vertical size of a character cell */
    uint8_t offset;     /*!< Value of first character in the font */
    uint8_t numchars;   /*!< Number of characters in the font */
};

EXTERN union
{
    /*! Structure describing the foreground color */
    struct
    {
        byte fclo;      /*!< Low byte of the foreground color */
        byte fchi;      /*!< High byte of the foreground color */
    } rgbPart;
    uint16_t fgcolor;
} stForeground;

/*! High byte of foreground color */
#define fch stForeground.rgbPart.fchi;
/*! Low byte of foreground color */
#define fcl stForeground.rgbPart.fclo;
/*! Full 16-bit foreground color */
#define fgc stForeground.fgcolor;

#ifdef SAVE_FOR_LATER
EXTERN struct
{
  long buffer1[4];
  byte bch;
  byte bcl;
  long buffer2;
  byte orient;
  long disp_x_size;
  long disp_y_size;
  byte bDisplayMode;
  byte display_transfer_mode;
  byte display_serial_mode;
  long buffer3[4];
} class_data;

#define bch class_data.bch;
#define bcl class_data.bcl;
#define orient class_data.orient;
#define disp_x_size class_data.disp_x_size;
#define disp_y_size class_data.disp_y_size;
#define bDisplayMode class_data.bDisplayMode;
#define display_transfer_mode class_data.display_transfer_mode;
#define display_serial_mode class_data.display_serial_mode;
#endif

//  EXTERN byte fch, fcl, bch, bcl;
EXTERN long Buffer1[4];
EXTERN byte bch,              /*!< High byte of the background color */
            bcl;              /*!< Low byte of the background color */
EXTERN long Buffer2[4];
EXTERN byte orient;           /*!< Current orientation */
EXTERN long Buffer3[4];
EXTERN long disp_x_size,      /*!< Number pixels in the horizontal direction */
            disp_y_size;      /*!< Number pixels in the vertical direction */
EXTERN long Buffer4[4];
EXTERN byte bDisplayModel, display_transfer_mode, display_serial_mode;

  //  regtype *P_RS, *P_WR, *P_CS, *P_RST, *P_SDA, *P_SCL, *P_ALE;
  //  regsize B_RS, B_WR, B_CS, B_RST, B_SDA, B_SCL, B_ALE;
EXTERN struct _current_font   cfont;    /*!< Current font */
EXTERN boolean _transparent;  /*!< True if text transparent */

EXTERN uint16_t IOX2status;
  

EXTERN uint16_t IOXold,       /*!< Previous value in I/O expander 2 latch */
                IOXnew;       /*!< Desired value in I/O expander 2 latch */


#ifdef	__cplusplus
}
#endif

#endif	/* CLASS_ATTRIBUTES_H */

