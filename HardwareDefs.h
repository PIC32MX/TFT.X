/*! \file  HardwareDefs.h
 *
 *  \brief Manifest constants for pin connections
 *
 * I/O expanders are on SPI2
 *
 *
 *  \author jjmcd
 *  \date December 14, 2013, 9:22 AM
 */
/*
 * Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#ifndef HARDWAREDEFS_H
#define	HARDWAREDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif

/*! I/O expander #1 Chip Select */
#define I1_CS _LATB11
/*! I/O expander #1 Chip Select TRIS */
#define I1_CS_TRIS _TRISB11
/*! I/O expander #2 Chip Select */
#define I2_CS _LATA4
/*! I/O expander #1 Chip Select TRIS */
#define I2_CS_TRIS _TRISA4
/*! SPI Input data line */
#define I_SI  _RB5
/*! SPI Input data line TRIS */
#define I_SI_TRIS  _TRISB5
/*! SPI Output data line */
#define I_SO  _LATB13
/*! SPI Output data line TRIS */
#define I_SO_TRIS  _TRISB13
/*1 SPI clock */
#define I_SCK _LATB15
/*1 SPI clock TRIS */
#define I_SCK_TRIS _TRISB15
/*! WR bit pin connection */
#define T_WR _LATB4
/*! WR bit data direction bit */
#define T_WR_TRIS _TRISB4
/*! RS bit pin connection */
#define T_RS _LATB14
/*! RS bit data direction bit */
#define T_RS_TRIS _TRISB14
/*! RT bit on mask */
#define T_RT_ONM 0x0010
/*! RS bit on mask */
#define T_RS_ONM 0x0008
//#define T_WR_ONM 0x0004
#define T_RD_ONM 0x0002
/*! CS bit on mask */
#define T_CS_ONM 0x0001
/*! RST bit on mask */
#define T_RST_ONM 0x0004
/*! RT bit off mask */
#define T_RT_OFM 0xffef
/*! RS bit off mask */
#define T_RS_OFM 0xfff7
//#define T_WR_OFM 0xfffb
/*! RD bit off mask */
#define T_RD_OFM 0xfffd
/*! CS bit off mask */
#define T_CS_OFM 0xfffe
/*! RST bit off mask */
#define T_RST_OFM 0xfffb

/* Touch pin definitions */
#define TT_IRQ_ONM 0x0100
#define TT_OUT_ONM 0x0200
#define TT_BSY_ONM 0x0400
#define TT_DIN_ONM 0x0800
#define TT_CS_ONM  0x1000
#define TT_CLK_ONM 0x2000

#define TT_IRQ_OFM 0xfeff
#define TT_OUT_OFM 0xfdff
#define TT_BSY_OFM 0xfbff
#define TT_DIN_OFM 0xf7ff
#define TT_CS_OFM  0xefff
#define TT_CLK_OFM 0xdfff

#define TT_DIR 0xc7f0
/* S17 #2 data direction */
//#define TT_DIR 0xc6f0

#define setWR() { T_WR = 1; }
#define clearWR() { T_WR = 0; }


#ifdef	__cplusplus
}
#endif

#endif	/* HARDWAREDEFS_H */

