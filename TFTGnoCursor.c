/*! \file  TFTGnoCursor.c
 *
 *  \brief Turn off the plotting cursor
 *
 *
 *  \author jjmcd
 *  \date June 18, 2015, 11:48 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFTGinternal.h"


/*! TFTGnoCursor - */

/*!
 *
 */
void TFTGnoCursor(void)
{
  nUseCursor = 0;
}
