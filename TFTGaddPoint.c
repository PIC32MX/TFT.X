/*! \file  TFTGaddPoint.c
 *
 *  \brief Add a point to an existing graph, simple
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:44 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFTG.h"
#include "TFTGinternal.h"


/*! TFTGaddPoint - Add a point to an existing graph */

/*! TFTGaddPoint() adds a point to a simple graph.  The caller passes a single
 *  value, the position of the point in pixels.  If the point exceeds the limits
 *  of the graphing area, the value is clipped to the graphing area.  A line
 *  is drawn from the previous point to the current point.
 *
 *  The row following the last row is filled with uFieldColor prior to drawing
 *  the line unless nEraseLast is zero.  The next row is changed to a different
 *  color unless nUseCursor is zero.
 *
 * \param int y pixel value to be plotted
 * \returns none
 */
void TFTGaddPoint(int y)
{
  /* Limit Y value to what will fit in field */
  if ( y<0 )
    y=0;
  if ( y>(TFTGYBOTTOM-TFTGYTOP) )
    y=(TFTGYBOTTOM-TFTGYTOP);

  /* Draw a line for all but the very first point */
  if ( nLastX!=0 )
    {
      if (nUseCursor)
        if ((nLastX + TFTGXLEFT + 2) < (TFTGXRIGHT - 1))
          {
            TFTsetColorX((uCursorColor));
            TFTline((nLastX + TFTGXLEFT + 2), TFTGYTOP, (nLastX + TFTGXLEFT + 2), TFTGYBOTTOM);
          }
      if (nEraseLast)
        {
          TFTsetColorX(uFieldColor);
          TFTline((nLastX + TFTGXLEFT + 1), TFTGYTOP, (nLastX + TFTGXLEFT + 1), TFTGYBOTTOM);
        }
      TFTsetColorX(uLineColor);
      TFTline( (nLastX+TFTGXLEFT), (TFTGYBOTTOM-nLastY), (nLastX+TFTGXLEFT+1), (TFTGYBOTTOM-y) );
    }
  else
    /* For the first point, clear the row but don't draw, just */
    {
      TFTsetColorX(uFieldColor);
      TFTline((TFTGXLEFT+1),TFTGYTOP,(TFTGXLEFT+1),TFTGYBOTTOM);
    }

  /* Remember the Y value for the start of the next line */
  nLastY=y;
  /* Bump the X, wrap if gone too far */
  nLastX++;
  if ( nLastX>(TFTGXRIGHT-TFTGXLEFT-2) )
    nLastX = 0;
}
