/*! \file  SSD1289defs.h
 *
 *  \brief Constants for the SSD1289 controller
 *
 *
 *  \author jjmcd
 *  \date February 27, 2014, 5:28 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#ifndef SSD1289DEFS_H
#define	SSD1289DEFS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define SSD1289_OSCEN   0x00
#define SSD1289_DRVOC   0x01
#define SSD1289_LCDAC   0x02
#define SSD1289_PWRC1   0x03
#define SSD1289_CMPR1   0x05
#define SSD1289_CMPR2   0x06
#define SSD1289_DPYCT   0x07
#define SSD1289_FCYCT   0x0b
#define SSD1289_PWRC2   0x0c
#define SSD1289_PWRC3   0x0d
#define SSD1289_PWRC4   0x0e
#define SSD1289_GASSP   0x0f
#define SSD1289_SLEEP   0x10
#define SSD1289_ENTRY   0x11
#define SSD1289_HPORC   0x16
#define SSD1289_VPORC   0x17
#define SSD1289_PWRC5   0x1e
#define SSD1289_WRITE   0x22
#define SSD1289_RWDM1   0x23
#define SSD1289_RWDM2   0x24
#define SSD1289_GAM01   0x30
#define SSD1289_GAM02   0x31
#define SSD1289_GAM03   0x32
#define SSD1289_GAM04   0x33
#define SSD1289_GAM05   0x34
#define SSD1289_GAM06   0x35
#define SSD1289_GAM07   0x36
#define SSD1289_GAM08   0x37
#define SSD1289_GAM09   0x3a
#define SSD1289_GAM10   0x3b




#define SSD1289_ON  1
#define SSD1289_OFF 0

#ifdef	__cplusplus
}
#endif

#endif	/* SSD1289DEFS_H */

