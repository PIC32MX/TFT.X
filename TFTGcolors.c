/*! \file  TFTGcolors.c
 *
 *  \brief Set the graph colors
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:53 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "TFTGinternal.h"


/*! TFTGcolors - Set the graph colors */

/*!
 *
 */
void TFTGcolors( unsigned int uTit,
        unsigned int uAnn,
        unsigned int uX,
        unsigned int uY,
        unsigned int uScreen,
        unsigned int uField,
        unsigned int uLine     )
{
  unsigned int r, g, b;

  uAnnotColor  = uAnn;
  uTitleColor  = uTit;
  uXLabelColor = uX;
  uYLabelColor = uY;
  uFieldColor  = uField;
  uScreenColor = uScreen;
  uLineColor   = uLine;
  r = uFieldColor&0xf800;
  if ( r>0xf000 )
    r = r - 0x4000;
  else
    r = r + 0x1000;
  g = uFieldColor&0x07e0;
  if ( g>0x04e0 )
    g = g - 0x0200;
  else
    g = g + 0x0040;
  b = uFieldColor&0x001f;
  if ( b>0x0010 )
    b = b - 0x0008;
  else
    b = b + 0x0002;
  uCursorColor = r|g|b;
}
