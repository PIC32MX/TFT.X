/*! \file  TFTroundRect.c
 *
 *  \brief Draw a rounded rectangle
 *
 *
 *  \author jjmcd
 *  \date February 26, 2014, 9:15 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */

#include "TFT.h"

#define swap(type, i, j) {type t = i; i = j; j = t;}


/*! TFTroundRect - Draw a rounded rectangle */

/*!
 *
 */
void TFTroundRect(int x1, int y1, int x2, int y2)
{
  int tmp;

  if (x1>x2)
    {
      swap(int, x1, x2);
    }
  if (y1>y2)
    {
      swap(int, y1, y2);
    }
  if ((x2-x1)>4 && (y2-y1)>4)
    {
      TFTpixel(x1+1,y1+1);
      TFTpixel(x2-1,y1+1);
      TFTpixel(x1+1,y2-1);
      TFTpixel(x2-1,y2-1);
      TFThLine(x1+2, y1, x2-x1-4);
      TFThLine(x1+2, y2, x2-x1-4);
      TFTvLine(x1, y1+2, y2-y1-4);
      TFTvLine(x2, y1+2, y2-y1-4);
    }
}


/*! TFTfillRoundRect - Draw a filled, rounded rectangle */

/*!
 *
 */
void TFTfillRoundRect(int x1, int y1, int x2, int y2)
{
  int tmp;
  int i;

  if (x1>x2)
    {
      swap(int, x1, x2);
    }
  if (y1>y2)
    {
      swap(int, y1, y2);
    }

  if ((x2-x1)>4 && (y2-y1)>4)
    {
      for (i=0; i<((y2-y1)/2)+1; i++)
	{
	  switch(i)
	    {
	    case 0:
	      TFThLine(x1+2, y1+i, x2-x1-4);
	      TFThLine(x1+2, y2-i, x2-x1-4);
	      break;
	    case 1:
	      TFThLine(x1+1, y1+i, x2-x1-2);
	      TFThLine(x1+1, y2-i, x2-x1-2);
	      break;
	    default:
	      TFThLine(x1, y1+i, x2-x1);
	      TFThLine(x1, y2-i, x2-x1);
	    }
	}
    }
}


