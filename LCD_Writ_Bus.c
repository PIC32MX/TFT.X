/*! \file  LCD_Writ_Bus.c
 *
 *  \brief Write to the TFT
 *
 *
 *  \author jjmcd
 *  \date January 8, 2014, 1:43 PM
 */
/* Software License Agreement
 *
 * Copyright (c) 2014 by John J. McDonough, WB8RCR and
 * 2010-2013 Henning Karlsen.
 *
 * This library is heavily based on the work of Henning Karlsen.
 * His UTFT library has been tailored for a special case of the
 * SainSoft 320x240 TFT using the SSD1289 controller, serialized
 * through a pair of MCP23S17 SPI I/O expanders.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the CC BY-NC-SA 3.0 license.
 * Please see the included documents for further information.
 */
#include <xc.h>
#include <stdint.h>
#include "HardwareDefs.h"
#include "MCP23S17defs.h"


void LCD_Writ_Bus(uint8_t VH, uint8_t VL )
{
  uint16_t word;

  word = (VH&0xff)<<8 | (VL&0xff);
  sendToS17(0,S17_OLATA,word);
  clearWR();
  setWR();
}

void LCD_Writ_Bus16(uint16_t V)
{
  //uint16_t word;
  //word = (VH&0xff)<<8 | (VL&0xff);
  sendToS17(0,S17_OLATA,V);
  clearWR();
  setWR();
}
