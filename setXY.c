/*! \file  setXY.c
 *
 *  \brief Define the working rectangle for the SSD1289
 *
 *
 *  \author jjmcd
 *  \date February 27, 2014, 3:29 PM
 */
/* Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include <xc.h>
#include <stdint.h>
#include "SSD1289defs.h"
#include "class_attributes.h"

#define word uint16_t

#define swap(type, i, j) {type t = i; i = j; j = t;}




/*! setXY - Set x and y boundaries for the SSD1289 */

/*!
 *
 */

void setXY(word x1, word y1, word x2, word y2)
{
        int tmp;

        if (orient==LANDSCAPE)
        {
                swap(word, x1, y1);
                swap(word, x2, y2)
                y1=disp_y_size-y1;
                y2=disp_y_size-y2;
                swap(word, y1, y2)
        }



        LCD_Write_COM_DATA(0x44,(x2<<8)+x1);    /* Horizontal RAM address position */
        LCD_Write_COM_DATA(0x45,y1);            /* Vertical RAM address start position */
        LCD_Write_COM_DATA(0x46,y2);            /* Vertical RAM address end position */
        LCD_Write_COM_DATA(0x4e,x1);            /* Set GDDRAM X address counter */
        LCD_Write_COM_DATA(0x4f,y1);            /* Set GDDRAM Y address counter */
        LCD_Write_COM(SSD1289_WRITE);                    /* RAM data write */

}

void clrXY( void )
{
        if (orient==PORTRAIT)
                setXY(0,0,disp_x_size,disp_y_size);
        else
                setXY(0,0,disp_y_size,disp_x_size);
}

