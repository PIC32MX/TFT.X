## Graphics TFT Library

This project provides a graphics library for the SainSmart 20-011-918
TFT.  The TFT is driven by a pair of Microchip MCP23S17 I/O expanders.

The PCB board layout and schematics may be found at
https://gitlab.com/PIC32MX/tft-board-pcb

